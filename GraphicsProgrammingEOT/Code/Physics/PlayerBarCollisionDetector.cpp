#include "pch.h"
#include "PlayerBarCollisionDetector.h"

#include "Code/Utils/SGWindowManager.h"
#include "Code/GameObjects/PlayerBar.h"

using namespace DirectX;
using namespace DirectX::SimpleMath;

PlayerBarCollisionDetector::PlayerBarCollisionDetector()
	: PlayerBarCollisionDetector(-1)
{ }

PlayerBarCollisionDetector::PlayerBarCollisionDetector(int id)
	: SGGameObject(id), playerBar(nullptr), windowManager(nullptr)
{ }

PlayerBar* PlayerBarCollisionDetector::GetPlayerBar()
{
	return playerBar;
}

void PlayerBarCollisionDetector::SetPlayerBar(PlayerBar* playerBar)
{
	this->playerBar = playerBar;
}

void PlayerBarCollisionDetector::SetWindowManager(SGWindowManager* windowManager)
{
	this->windowManager = windowManager;
}

void PlayerBarCollisionDetector::Update(float deltaTime)
{
	Vector2 nextPosition = playerBar->GetMaxSpeed() >= 0
		? playerBar->GetPosition() + playerBar->GetVelocity() * deltaTime
		: playerBar->GetPosition() + playerBar->GetVelocity();

	float xLimit = (windowManager->GetWidth() - playerBar->GetWidth()) / 2;
	if (nextPosition.x > xLimit)
	{
		playerBar->SetVelocity(Vector2::Zero);
		playerBar->SetPosition(Vector2(xLimit, playerBar->GetPosition().y));
	}
	if (nextPosition.x < -xLimit)
	{
		playerBar->SetVelocity(Vector2::Zero);
		playerBar->SetPosition(Vector2(-xLimit, playerBar->GetPosition().y));
	}
}
