#pragma once

#include "Code/GameObjects/SGGameObject.h"

#include <vector>

class Ball;
class SGWindowManager;

//optimized to check collision between a spherical object and rectangular ones
class BallCollisionDetector : public SGGameObject
{
public:
	BallCollisionDetector();
	BallCollisionDetector(int id);

	Ball* GetBall();
	void SetBall(Ball* ball);
	void SetSceneObjects(std::vector<SGGameObject*>* sceneObjects);

protected:
	virtual void Update(float deltaTime) override;

	DirectX::SimpleMath::Vector2 CalculateDirectionChange(DirectX::SimpleMath::Vector2 ballNextPosition, const SGGameObject& collidingObject);
	bool CheckOverlap(float center1, float center2, float radius1, float radius2);

	Ball* ball;
	std::vector<SGGameObject*>* sceneObjects;
};

