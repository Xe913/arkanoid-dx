#pragma once

#include "Code/GameObjects/SGGameObject.h"

#include <vector>

class PlayerBar;
class SGWindowManager;

class PlayerBarCollisionDetector : public SGGameObject
{
public:
	PlayerBarCollisionDetector();
	PlayerBarCollisionDetector(int id);

	PlayerBar* GetPlayerBar();
	void SetPlayerBar(PlayerBar* playerBar);
	void SetWindowManager(SGWindowManager* windowManager);

private:
	virtual void Update(float deltaTime) override;

	PlayerBar* playerBar;
	SGWindowManager* windowManager;
};

