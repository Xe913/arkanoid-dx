#include "pch.h"
#include "SGCollisionFramework.h"

#include "Code/GameObjects/SGGameObject.h"

using namespace std;
using namespace DirectX;
using namespace DirectX::SimpleMath;

//----------------------------------------------------------------------------------------
//write here to decide what interacts with what. The method is called when the game begins


void CollisionMatrix::InitializeMatrix()
{
	collisionMatrix = vector<vector<CollisionType>>((int)CollisionLayer::Other, vector<CollisionType>((int)CollisionLayer::Other, CollisionType::None));

	collisionMatrix[(int)CollisionLayer::Blocks][(int)CollisionLayer::Blocks] = CollisionType::Collide;
	collisionMatrix[(int)CollisionLayer::Blocks][(int)CollisionLayer::Ball] = CollisionType::Collide;
	collisionMatrix[(int)CollisionLayer::Ball][(int)CollisionLayer::Blocks] = CollisionType::Collide;
	collisionMatrix[(int)CollisionLayer::Player][(int)CollisionLayer::Ball] = CollisionType::Collide;
	collisionMatrix[(int)CollisionLayer::Ball][(int)CollisionLayer::Player] = CollisionType::Collide;
}

//----------------------------------------------------------------------------------------

CollisionType CollisionMatrix::AreObjectsColliding(const SGGameObject& object1, const SGGameObject& object2)
{
	CollisionType collisionType = CheckCollisionType(object1, object2);
	if (collisionType != CollisionType::None && IsPositionOverlapping(object1, object2))
		return collisionType;
	else
		return CollisionType::None;
}


bool CollisionMatrix::IsPositionOverlapping(const SGGameObject& object1, const SGGameObject& object2)
{
	return IsPositionOverlapping(object1.GetPositionRect(), object2.GetPositionRect());
}

bool CollisionMatrix::IsPositionOverlapping(RECT rect1, RECT rect2)
{
	RECT intersection = Rectangle::Intersect(rect1, rect2);
	return intersection.top != 0
		|| intersection.bottom != 0
		|| intersection.left != 0
		|| intersection.right != 0;
}

CollisionType CollisionMatrix::CheckCollisionType(const SGGameObject& object1, const SGGameObject& object2)
{
	return CheckCollisionType(object1.GetLayer(), object2.GetLayer());
}

CollisionType CollisionMatrix::CheckCollisionType(CollisionLayer layer1, CollisionLayer layer2)
{
	return collisionMatrix[(int)layer1][(int)layer2];
}

bool CollisionMatrix::CanCollide(CollisionLayer layer1, CollisionLayer layer2)
{
	return CollisionMatrix::CheckCollisionType(layer1, layer2) == CollisionType::Collide;
}

bool CollisionMatrix::CanOverlap(CollisionLayer layer1, CollisionLayer layer2)
{
	return CollisionMatrix::CheckCollisionType(layer1, layer2) == CollisionType::Overlap;
}