#pragma once

#include <vector>

class SGGameObject;

enum class CollisionLayer
{
	None,
	Player,
	Ball,
	Blocks,
	Scene,
	UI,
	Other //other is used to get the collision matrix size and should always be the last value
};

enum class CollisionType
{
	Overlap,
	Collide,
	None //same for none
};

namespace
{
	static std::vector<std::vector<CollisionType>> collisionMatrix;
}

class CollisionMatrix
{
public:
	//when customizing the matrix for an upgrade or a different project
	//just rewrite this method to setup the desired collisions
	static void InitializeMatrix();

	static CollisionType AreObjectsColliding(const SGGameObject& object1, const SGGameObject& object2);

	static bool IsPositionOverlapping(const SGGameObject& object1, const SGGameObject& object2);
	static bool IsPositionOverlapping(RECT rect1, RECT rect2);
	static CollisionType CheckCollisionType(const SGGameObject& object1, const SGGameObject& object2);
	static CollisionType CheckCollisionType(CollisionLayer layer1, CollisionLayer layer2);

	static bool CanCollide(CollisionLayer layer1, CollisionLayer layer2);
	static bool CanOverlap(CollisionLayer layer1, CollisionLayer layer2);
};