#include "pch.h"
#include "BallCollisionDetector.h"

#include "Code/Utils/ArkanoidConstants.h"
#include "Code/Utils/SGWindowManager.h"
#include "Code/GameObjects/Ball.h"
#include "Code/GameObjects/Block.h"

#include <random>

using namespace std;
using namespace DirectX;
using namespace DirectX::SimpleMath;

BallCollisionDetector::BallCollisionDetector()
	: BallCollisionDetector(-1)
{ }

BallCollisionDetector::BallCollisionDetector(int id)
	: SGGameObject(id), ball(nullptr), sceneObjects(nullptr)
{ }

Ball* BallCollisionDetector::GetBall()
{
	return ball;
}

void BallCollisionDetector::SetBall(Ball* ball)
{
	this->ball = ball;
}

void BallCollisionDetector::SetSceneObjects(std::vector<SGGameObject*>* sceneObjects)
{
	this->sceneObjects = sceneObjects;
}

void BallCollisionDetector::Update(float deltaTime)
{
	//right now there's no way to change the ball movement system to "snap to place"
	//but it might happen in the future
	Vector2 ballNextPosition = ball->GetMaxSpeed() >= 0
		? ball->GetPosition() + ball->GetVelocity() * deltaTime
		: ball->GetPosition() + ball->GetVelocity();

	if (ball != nullptr)
	{
		if (sceneObjects != nullptr)
		{
			for (int i = 0; i < sceneObjects->size(); i++)
			{
				SGGameObject* other = sceneObjects->at(i);
				if (other->CanCollide() && CollisionMatrix::AreObjectsColliding(*ball, *other) == CollisionType::Collide)
				{
					Vector2 newDirection = CalculateDirectionChange(ballNextPosition, *other);

					random_device rd;
					mt19937 rng(rd());
					uniform_real_distribution<float> rnd_x(-BALL_BOUNCE_VARIATION, BALL_BOUNCE_VARIATION);
					uniform_real_distribution<float> rnd_y(-BALL_BOUNCE_VARIATION, BALL_BOUNCE_VARIATION);
					newDirection *= Vector2(1 + rnd_x(rng), 1 + rnd_y(rng));
					newDirection.Normalize();

					ball->SetDirection(newDirection);
					other->TryOnCollision(*ball);

					break; //avoid strange behavior when the ball can hit multiple blocks in one frame. Could cause problems in some corner cases tho
				}
			}
		}
	}
}

Vector2 BallCollisionDetector::CalculateDirectionChange(Vector2 ballNextPosition, const SGGameObject& collidingObject)
{
	Vector2 centerToCenter = ballNextPosition - collidingObject.GetPosition();
	centerToCenter.Normalize();
	float ballCornerDotInfo = centerToCenter.y;
	float blockCornerDotInfo = collidingObject.GetCornerDotInfo();
	Vector2 directionChange = Vector2::One;
	//if dot(ball-blockCenter, up) >= dot(blockCorner-blockCenter, up), collision happens on the block top side
	//if dot(ball-cent, up) <= -dot(cent-corner, up), it happens on the bottom side
	if (ballCornerDotInfo >= blockCornerDotInfo || ballCornerDotInfo <= -blockCornerDotInfo) 
	{
		directionChange.y = -1;
	}
	else //else, it's on the left side if the ball is on the left, on the right otherwise
	{
		directionChange.x = -1;
	}
	return ball->GetDirection() * directionChange;
/*
// 	Vector2 otherPosition = collidingObject.GetPosition();
// 	float ballRadius = ball->GetWidth() / 2;
// 	float otherWidth = collidingObject.GetWidth() / 2;
// 	float otherHeight = collidingObject.GetHeight() / 2;
// 
// 	bool horizontalOverlap = CheckOverlap(ballNextPosition.x, otherPosition.x, ballRadius, otherWidth);
// 	bool verticalOverlap = CheckOverlap(ballNextPosition.y, otherPosition.y, ballRadius, otherHeight);
// 
// 	if (horizontalOverlap && verticalOverlap)
// 	{
// 		Vector2 directionChange = Vector2::One;
// 		if (ballNextPosition.x < otherPosition.x - otherWidth || ballNextPosition.x > otherPosition.x + otherWidth) directionChange.x = -1;
// 		if (ballNextPosition.y < otherPosition.y - otherHeight || ballNextPosition.y > otherPosition.y + otherHeight) directionChange.y = -1;
// 		ball->SetDirection(ball->GetDirection() * directionChange);
// 	}
// 
// 	return horizontalOverlap && verticalOverlap;
*/
}

bool BallCollisionDetector::CheckOverlap(float center1, float center2, float radius1, float radius2)
{
	return (center1 <= center2 && center1 + radius1 >= center2 - radius2)
		|| (center1 > center2 && center1 - radius1 <= center2 + radius2);
}
