#include "pch.h"
#include "SGGameObject.h"

using namespace DirectX;
using namespace DirectX::SimpleMath;

SGGameObject::SGGameObject(int objectID)
	: objectID(objectID), position(Vector2::Zero),
	canPreUpdate(false), canUpdate(false), canMove(false),
	hasGraphics(false), canCollide(false),
	layer(CollisionLayer::None), rect(RECT()), center(Vector2::Zero),
	velocity(Vector2::Zero), maxSpeed(0), color(Colors::White),
	cornerDotInfo(0)
{ }

void SGGameObject::TryUpdate(float deltaTime)
{
	if (canUpdate) Update(deltaTime);
}

void SGGameObject::Update(float deltaTime)
{ }

void SGGameObject::TryPreUpdate(float deltaTime)
{
	if (canPreUpdate) PreUpdate(deltaTime);
}

void SGGameObject::PreUpdate(float deltaTime)
{ }

void SGGameObject::TryMove(float deltaTime)
{
	if (canMove) Move(deltaTime);
}

void SGGameObject::Move(float deltaTime)
{
	if (maxSpeed >= 0) position += velocity * deltaTime;
	else position += velocity;
}

void SGGameObject::TryRender(SpriteBatch& spriteBatch, Vector2 screenPosition)
{
	if (hasGraphics) Render(spriteBatch, screenPosition);
}

void SGGameObject::Render(SpriteBatch& spriteBatch, Vector2 screenPosition)
{
	spriteBatch.Draw(srv.Get(), screenPosition, &rect,
		color, 0.f, position + center);
}

void SGGameObject::SetupGraphics(ID3D11Device& device, wchar_t* path)
{
	DX::ThrowIfFailed(
		CreateWICTextureFromFile(
			&device, path,
			resource.GetAddressOf(),
			srv.ReleaseAndGetAddressOf()));

	DX::ThrowIfFailed(resource.As(&texture));
	texture->GetDesc(&description);
}

void SGGameObject::ResetSRV()
{
	srv.Reset();
}

int SGGameObject::GetObjectID() const
{
	return objectID;
}

DirectX::SimpleMath::Vector2 SGGameObject::GetPosition() const
{
	return position;
}

float SGGameObject::GetWidth() const
{
	return rect.right;
}

float SGGameObject::GetHeight() const
{
	return rect.bottom;
}

DirectX::SimpleMath::Vector2 SGGameObject::GetDimensions() const
{
	return DirectX::SimpleMath::Vector2(rect.right, rect.bottom);
}

RECT SGGameObject::GetPositionRect() const
{
	RECT positionRect = RECT();
	positionRect.left = position.x - rect.right / 2;
	positionRect.right = position.x + rect.right / 2;
	positionRect.top = position.y - rect.bottom / 2;
	positionRect.bottom = position.y + rect.bottom / 2;
	return positionRect;
}

bool SGGameObject::CanPreUpdate() const
{
	return canPreUpdate;
}

bool SGGameObject::CanUpdate() const
{
	return canUpdate;
}

bool SGGameObject::HasGraphics() const
{
	return hasGraphics;
}

DirectX::XMVECTORF32 SGGameObject::GetColor() const
{
	return color;
}

bool SGGameObject::CanMove() const
{
	return canMove;
}

bool SGGameObject::CanCollide() const
{
	return canCollide;
}

CollisionLayer SGGameObject::GetLayer() const
{
	return layer;
}

void SGGameObject::SetPosition(Vector2 position)
{
	this->position = position;
}

void SGGameObject::SetWidth(float width)
{
	rect.right = width;
	center.x = width / 2;
	UpdateCornerDotInfo();
}

void SGGameObject::SetHeight(float height)
{
	rect.bottom = height;
	center.y = height / 2;
	UpdateCornerDotInfo();
}

void SGGameObject::SetDimensions(float width, float height)
{
	SetWidth(width);
	SetHeight(height);
}

void SGGameObject::SetDimensions(Vector2 dimensions)
{
	SetDimensions(dimensions.x, dimensions.y);
}

void SGGameObject::SetCanPreUpdate(bool canPreUpdate)
{
	this->canPreUpdate = canPreUpdate;
}

void SGGameObject::SetCanUpdate(bool canUpdate)
{
	this->canUpdate = canUpdate;
}

void SGGameObject::SetHasGraphics(bool hasGraphics)
{
	this->hasGraphics = hasGraphics;
}

void SGGameObject::SetColor(DirectX::XMVECTORF32 color)
{
	this->color = color;
}

void SGGameObject::SetCanMove(bool canMove)
{
	this->canMove = canMove;
	if (!this->canMove) velocity = Vector2::Zero;
}

void SGGameObject::SetCanCollide(bool canCollide)
{
	this->canCollide = canCollide;
}

void SGGameObject::SetLayer(CollisionLayer layer)
{
	this->layer = layer;
}

DirectX::SimpleMath::Vector2 SGGameObject::GetVelocity()
{
	return velocity;
}

DirectX::SimpleMath::Vector2 SGGameObject::GetDirection()
{
	Vector2 direction = velocity;
	direction.Normalize();
	return direction;
}

float SGGameObject::GetSpeed()
{
	return velocity.Length();
}

float SGGameObject::GetMaxSpeed()
{
	return maxSpeed;
}

void SGGameObject::SetVelocity(DirectX::SimpleMath::Vector2 velocity)
{
	if (maxSpeed >= 0 && velocity.LengthSquared() > maxSpeed * maxSpeed)
	{
		velocity.Normalize();
		velocity *= maxSpeed;
	}
	this->velocity = velocity;
}

void SGGameObject::SetDirection(DirectX::SimpleMath::Vector2 direction)
{
	SetVelocity(direction * GetSpeed());
}

void SGGameObject::SetSpeed(float speed)
{
	if (speed > maxSpeed) speed = maxSpeed;
	SetVelocity(GetDirection() * maxSpeed);
}

void SGGameObject::SetMaxSpeed(float maxSpeed)
{
	this->maxSpeed = maxSpeed;
}

void SGGameObject::TryOnCollision(SGGameObject& otherObject)
{
	if (canCollide) OnCollision(otherObject);
}

void SGGameObject::OnCollision(SGGameObject& otherObject) { }

float SGGameObject::GetCornerDotInfo() const
{
	return cornerDotInfo;
}

void SGGameObject::UpdateCornerDotInfo()
{
	Vector2 centerToCornerDirection = GetDimensions() / 2;
	centerToCornerDirection.Normalize(); //direction vector from the center to the top-left corner

	//a = center to corner direction, b = up (0, 1)
	//cos(angle)|a||b| = ax*bx + ay*by
	//since |a| = |b| = 1, ax = 0, ay = 1
	//cos(angle) = by
	//when checking collision, one can get the rect colliding side by comparing
	//center-to-other-obj-center-direction.y to this value
	cornerDotInfo = centerToCornerDirection.y;
}
