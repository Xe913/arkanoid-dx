#pragma once

#include "Code/GameObjects/SGGameObject.h"

class PlayerBar : public SGGameObject
{
public:
	PlayerBar(int id);

	void GoToTargetPosition(DirectX::SimpleMath::Vector2 targetPosition);

	void LockHorizontalMovement();
	void LockVerticalMovement();
	void UnlockHorizontalMovement();
	void UnlockVerticalMovement();

	bool IsControlOn();
	void ControlOn();
	void ControlOff();

protected:
	void Move(float deltaTime) override;

	bool horizontalMovementAllowed;
	bool verticalMovementAllowed;

	DirectX::SimpleMath::Vector2 targetPosition;
};

