#include "pch.h"
#include "PlayerBar.h"

#include "Code/Utils/ArkanoidConstants.h"

using namespace DirectX;
using namespace DirectX::SimpleMath;

PlayerBar::PlayerBar(int id)
	: SGGameObject(id),
	horizontalMovementAllowed(true),
	verticalMovementAllowed(true)
{ }

void PlayerBar::GoToTargetPosition(DirectX::SimpleMath::Vector2 targetPosition)
{
	if (!horizontalMovementAllowed) targetPosition.x = position.x;
	if (!verticalMovementAllowed) targetPosition.y = position.y;
	SetPosition(targetPosition);
}

void PlayerBar::LockHorizontalMovement()
{
	horizontalMovementAllowed = false;
	SetCanMove(horizontalMovementAllowed || verticalMovementAllowed);
}

void PlayerBar::LockVerticalMovement()
{
	verticalMovementAllowed = false;
	SetCanMove(horizontalMovementAllowed || verticalMovementAllowed);
}

void PlayerBar::UnlockHorizontalMovement()
{
	horizontalMovementAllowed = true;
	SetCanMove(horizontalMovementAllowed || verticalMovementAllowed);
}

void PlayerBar::UnlockVerticalMovement()
{
	verticalMovementAllowed = true;
	SetCanMove(horizontalMovementAllowed || verticalMovementAllowed);
}

void PlayerBar::Move(float deltaTime)
{
	if (!horizontalMovementAllowed) velocity.x = 0.f;
	if (!verticalMovementAllowed) velocity.y = 0.f;
	__super::Move(deltaTime);
}

bool PlayerBar::IsControlOn()
{
	return __super::CanMove();
}

void PlayerBar::ControlOn()
{
	__super::SetCanMove(true);
}

void PlayerBar::ControlOff()
{
	__super::SetCanMove(false);
}