#pragma once

#include "Code/GameObjects/SGGameObject.h"

class SGWindowManager;

class Ball : public SGGameObject
{
public:
	Ball(int id);

	void SetWindowManager(SGWindowManager* windowManager);

	static DirectX::SimpleMath::Vector2 Ball::GetRandomStartingPosition(DirectX::SimpleMath::Vector2 screenDimensions);
	static DirectX::SimpleMath::Vector2 Ball::GetRandomStartingPosition(float screenWidth, float screenHeigth);
	static DirectX::SimpleMath::Vector2 Ball::GetRandomStartingDirection();

private:
	SGWindowManager* windowManager;
};