#pragma once

#include "Code/Physics/SGCollisionFramework.h"

class SGGameObject
{

public:
	SGGameObject(int objectID);

	void TryPreUpdate(float deltaTime);
	void TryMove(float deltaTime);
	void TryUpdate(float deltaTime);
	void TryRender(DirectX::SpriteBatch& spriteBatch, DirectX::SimpleMath::Vector2 screenPosition);

	void SetupGraphics(ID3D11Device& device, wchar_t* path);
	void ResetSRV();

	//getters
	int GetObjectID() const;
	DirectX::SimpleMath::Vector2 GetPosition() const;
	float GetWidth() const;
	float GetHeight() const;
	DirectX::SimpleMath::Vector2 GetDimensions() const;
	RECT GetPositionRect() const;
	bool CanPreUpdate() const;
	bool CanUpdate() const;
	bool HasGraphics() const;
	DirectX::XMVECTORF32 GetColor() const;
	bool CanMove() const;
	bool CanCollide() const;
	CollisionLayer GetLayer() const;

	//setters
	void SetPosition(DirectX::SimpleMath::Vector2);
	void SetWidth(float width);
	void SetHeight(float height);
	void SetDimensions(float width, float height);
	void SetDimensions(DirectX::SimpleMath::Vector2 dimensions);
	void SetCanPreUpdate(bool canPreUpdate);
	void SetCanUpdate(bool canUpdate);
	void SetHasGraphics(bool hasGraphics);
	void SetColor(DirectX::XMVECTORF32 color);
	void SetCanMove(bool canMove);
	void SetCanCollide(bool canCollide);
	void SetLayer(CollisionLayer layer);

	//movement
	DirectX::SimpleMath::Vector2 GetVelocity();
	DirectX::SimpleMath::Vector2 GetDirection();
	float GetSpeed();
	float GetMaxSpeed();

	void SetVelocity(DirectX::SimpleMath::Vector2 velocity);
	void SetDirection(DirectX::SimpleMath::Vector2 direction);
	void SetSpeed(float speed);
	void SetMaxSpeed(float maxSpeed);

	//physics
	void TryOnCollision(SGGameObject & otherObject);

	//for optimization purposes. See cornerDotInfo below
	float GetCornerDotInfo() const;

protected:
	virtual void PreUpdate(float deltaTime);
	virtual void Move(float deltaTime);
	virtual void Update(float deltaTime);
	virtual void Render(DirectX::SpriteBatch& spriteBatch, DirectX::SimpleMath::Vector2 screenPosition);
	virtual void OnCollision(SGGameObject& otherObject);

	//for optimization purposes. See cornerDotInfo below
	void UpdateCornerDotInfo();

	int objectID;
	DirectX::SimpleMath::Vector2 position;
	DirectX::SimpleMath::Vector2 center;
	bool canPreUpdate;
	bool canUpdate;
	bool hasGraphics;
	DirectX::XMVECTORF32 color;
	bool canMove;
	bool canCollide;
	CollisionLayer layer;

	//graphics
	Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> srv;

	Microsoft::WRL::ComPtr<ID3D11Resource> resource;
	Microsoft::WRL::ComPtr<ID3D11Texture2D> texture;
	CD3D11_TEXTURE2D_DESC description;
	RECT rect;

	//movement
	DirectX::SimpleMath::Vector2 velocity;
	float maxSpeed;
	
	//used when checking collisions. It's stored on dimensions change
	//to avoid calculating it every time a collision is checked.
	float cornerDotInfo;
};

