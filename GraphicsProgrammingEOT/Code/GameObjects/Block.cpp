#include "pch.h"
#include "Block.h"

#include "BlocksTracker.h"

using namespace std;
using namespace DirectX;

Block::Block(int id)
	: SGGameObject(id)
{
	hits = 0;
	score = 0;
}

int Block::RemoveHitPoint()
{
	SetHits(--hits);
	return GetHits();
}

void Block::AddTracker(BlocksTracker* tracker)
{
	this->tracker = tracker;
}

vector<XMVECTORF32> Block::GetColorSet()
{
	return colorSet;
}

XMVECTORF32 Block::GetColorFromIndex(int index)
{
	return colorSet[index];
}

int Block::GetHits()
{
	return hits + 1;
}

int Block::GetScore()
{
	return score;
}

void Block::SetColorSet(std::vector<DirectX::XMVECTORF32> colorSet)
{
	this->colorSet = colorSet;
	UpdateColor();
}

void Block::SetColorAtIndex(XMVECTORF32 color, int index)
{
	if (index < colorSet.size()) colorSet[index] = color;
	UpdateColor();
}

void Block::SetHits(int hits)
{
	this->hits = hits - 1;
	UpdateColor();
	if (hits < 0)
	{
		SetHasGraphics(false);
		SetCanCollide(false);
		tracker->BlockDestroyed();
	}
}

void Block::SetScore(int score)
{
	this->score = score;
}

void Block::UpdateColor()
{
	int colorIndex = hits;
	if (colorIndex < 0) colorIndex = 0;
	if (colorIndex >= colorSet.size()) colorIndex = colorSet.size() - 1;
	SetColor(colorSet[colorIndex]);
}

void Block::OnCollision(SGGameObject& otherObject)
{
	RemoveHitPoint();
}
