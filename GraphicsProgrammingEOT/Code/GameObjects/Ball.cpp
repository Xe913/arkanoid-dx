#include "pch.h"
#include "Ball.h"

#include "Code/Utils/ArkanoidConstants.h"
#include "Code/Utils/SGWindowManager.h"

#include <random>

using namespace std;
using namespace DirectX;
using namespace DirectX::SimpleMath;

Ball::Ball(int id)
	: SGGameObject(id)
{ }

void Ball::SetWindowManager(SGWindowManager* windowManager)
{
	this->windowManager = windowManager;
}
DirectX::SimpleMath::Vector2 Ball::GetRandomStartingPosition(DirectX::SimpleMath::Vector2 screenDimensions)
{
	return GetRandomStartingPosition(screenDimensions.x, screenDimensions.y);
}

Vector2 Ball::GetRandomStartingPosition(float screenWidth, float screenHeigth)
{
	random_device rd;
	mt19937 rng(rd());
	uniform_int_distribution<int> rnd(screenWidth * 0.1, screenWidth * 0.9);
	return Vector2(rnd(rng), BALL_RELATIVE_STARTING_POS_X * screenHeigth);
}

Vector2 Ball::GetRandomStartingDirection()
{
	random_device rd;
	mt19937 rng(rd());
	uniform_real_distribution<float> rnd(-BALL_LAUNCH_VARIATION, BALL_LAUNCH_VARIATION);
	Vector2 ballStartingDirection = Vector2(rnd(rng), 0.5);
	ballStartingDirection.Normalize();
	return ballStartingDirection;
}