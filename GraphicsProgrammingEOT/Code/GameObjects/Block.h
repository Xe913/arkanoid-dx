#pragma once

#include "Code/GameObjects/SGGameObject.h"

class BlocksTracker;

class Block : public SGGameObject
{
public:
	Block(int id);

	int RemoveHitPoint();
	void AddTracker(BlocksTracker* tracker);

	std::vector<DirectX::XMVECTORF32> GetColorSet();
	DirectX::XMVECTORF32 GetColorFromIndex(int index);
	int GetHits();
	int GetScore();

	void SetColorSet(std::vector<DirectX::XMVECTORF32> colorSet);
	void SetColorAtIndex(DirectX::XMVECTORF32 color, int index);
	void SetHits(int hits);
	void SetScore(int score);

	void UpdateColor();

private:
	virtual void OnCollision(SGGameObject& otherObject) override;

	BlocksTracker* tracker;
	std::vector<DirectX::XMVECTORF32> colorSet;

	int hits;
	int score;
};

