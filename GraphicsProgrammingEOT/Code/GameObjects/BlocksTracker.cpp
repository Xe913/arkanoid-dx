#include "pch.h"
#include "BlocksTracker.h"

BlocksTracker::BlocksTracker(int id)
	: SGGameObject(id), blocks(999), currentBlocks(999)
{ }

void BlocksTracker::SetNumberOfBlocks(int blocks)
{
	this->blocks = blocks;
	this->currentBlocks = blocks;
}

int BlocksTracker::GetNumberOfBlocks()
{
	return blocks;
}

int BlocksTracker::GetCurrentNumberOfBlocks()
{
	return currentBlocks;
}

void BlocksTracker::BlockDestroyed()
{
	if (currentBlocks > 0) currentBlocks--;
}
