#include "pch.h"
#include "PlayerController.h"
#include "Code/Utils/SGWindowManager.h"

using namespace std;
using namespace DirectX;
using namespace DirectX::SimpleMath;

PlayerController::PlayerController(int id)
	: SGGameObject(id), controlType(ControlType::NotSet),
	mouse(nullptr), keyboard(nullptr), windowManager(nullptr)
{ }

void PlayerController::SetWindowManager(SGWindowManager* windowManager)
{
	this->windowManager = windowManager;
}

void PlayerController::SetControlSystem(DirectX::Mouse& mouse)
{
	controlType = ControlType::Mouse;
	this->mouse = &mouse;
}

void PlayerController::SetControlSystem(DirectX::Keyboard& keyboard)
{
	controlType = ControlType::Keyboard;
	this->keyboard = &keyboard;
}

void PlayerController::AddControlledObject(SGGameObject* controlledObject)
{
	if (controlledObjects.size() > 0)
	{
		if (find(controlledObjects.begin(), controlledObjects.end(), controlledObject) == controlledObjects.end())
		{
			controlledObjects.push_back(controlledObject);
		}
	}
	else
	{
		controlledObjects.push_back(controlledObject);
	}
}

void PlayerController::ResetControlledObjects()
{
	controlledObjects.clear();
}

void PlayerController::PreUpdate(float deltaTime)
{
	if (controlType == ControlType::Mouse)
	{
		Mouse::State mouseState = mouse->GetState();
		Vector2 mouseScreenPos = windowManager->GetPositionOnScreen(Vector2(mouseState.x, mouseState.y));
		for (int i = 0; i < controlledObjects.size(); i++)
		{
			Vector2 selfToMouse = mouseScreenPos - controlledObjects[i]->GetPosition();
			controlledObjects[i]->SetVelocity(selfToMouse);
		}
	}
}

ControlType PlayerController::GetControlType()
{
	return controlType;
}

//you can swap control type only if the corresponding control system has been set
void PlayerController::SetControlType(ControlType controlType)
{
	switch (controlType)
	{
	case ControlType::Mouse:
		if (mouse != nullptr) this->controlType = controlType;
		break;
	case ControlType::Keyboard:
		if (keyboard != nullptr) this->controlType = controlType;
		break;
	default:
		this->controlType = ControlType::NotSet;
	}
}

