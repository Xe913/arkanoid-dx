#include "pch.h"
#include "GameOverBar.h"

#include "Code/Utils/SGWindowManager.h"
#include "Code/GameObjects/Ball.h"
#include "Code/GameObjects/LivesTracker.h"

using namespace DirectX;
using namespace DirectX::SimpleMath;

GameOverBar::GameOverBar(int id)
	: SGGameObject(id), livesTracker(nullptr)
{ }

void GameOverBar::SetLivesTracker(LivesTracker * livesTracker)
{
	this->livesTracker = livesTracker;
}

void GameOverBar::SetWindowManager(SGWindowManager* windowManager)
{
	this->windowManager = windowManager;
}

void GameOverBar::OnCollision(SGGameObject& other)
{
	Ball* ball = dynamic_cast<Ball*>(&other);
	//considering movement limitation performed by other classes
	//this object can realistically collide only with the ball
	//however, this redundant check is performed in case the
	//game gets bigger and more objects become able to collide
	//with the bottom of the screen
	if (ball != nullptr)
	{
		int lives = livesTracker->RemoveLife();
		Vector2 respawnPosition = Ball::GetRandomStartingPosition(windowManager->GetDimensions());
		Vector2 respawnDirection = Ball::GetRandomStartingDirection();
		ball->SetPosition(windowManager->GetPositionOnScreen(respawnPosition));
		ball->SetDirection(respawnDirection);
	}
}
