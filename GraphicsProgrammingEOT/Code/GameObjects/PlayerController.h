#pragma once

#include "Code/GameObjects/SGGameObject.h"

enum class ControlType
{
	Mouse,
	Keyboard,
	NotSet
};

class SGWindowManager;

class PlayerController : public SGGameObject
{
public:
	PlayerController(int id);

	void SetWindowManager(SGWindowManager* windowManager);
	void SetControlSystem(DirectX::Mouse& mouse);
	void SetControlSystem(DirectX::Keyboard& keyboard);

	void AddControlledObject(SGGameObject* controlledObject);
	void ResetControlledObjects();

	ControlType GetControlType();
	void SetControlType(ControlType controlType);

protected:
	virtual void PreUpdate(float deltaTime) override;

	ControlType controlType;
	SGWindowManager* windowManager;
	DirectX::Mouse* mouse;
	DirectX::Keyboard* keyboard;

	std::vector<SGGameObject*> controlledObjects;
};

