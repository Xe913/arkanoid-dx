#include "pch.h"
#include "LivesTracker.h"

#include "Code/Utils/ArkanoidConstants.h"

LivesTracker::LivesTracker(int id)
	: SGGameObject(id), lifeIcons(nullptr),
	startingLives(STARTING_LIVES), lives(STARTING_LIVES)
{ }

int LivesTracker::GetCurrentLives()
{
	return lives;
}

int LivesTracker::AddLife()
{
	if (lives - 1 >= 0 && lives - 1 < lifeIcons->size())
	{
		SGGameObject* lifeIcon = lifeIcons->at(lives - 1);
		lifeIcon->SetHasGraphics(true);
	}
	if (lives < MAX_DISPLAYABLE_LIVES) lives++;
	return lives;
}

int LivesTracker::RemoveLife()
{
	if (lives - 1 >= 0 && lives - 1 < lifeIcons->size())
	{
		SGGameObject* lifeIcon = lifeIcons->at(lives - 1);
		lifeIcon->SetHasGraphics(false);
	}
	if (lives >= 0) lives--;
	return lives;
}

int LivesTracker::ResetStartingLives()
{
	lives = startingLives;
	if (lifeIcons != nullptr)
	{
		for (int i = 0; i < lifeIcons->size(); i++)
		{
			SGGameObject* lifeIcon = lifeIcons->at(i);
			lifeIcon->SetHasGraphics(i < lives);
		}
	}
	return lives;
}

void LivesTracker::SetStartingLives(int startingLives)
{
	this->startingLives = startingLives;
}

void LivesTracker::SetLifeIcons(std::vector<Block*>* lifeIcons)
{
	this->lifeIcons = lifeIcons;
}
