#pragma once

#include "SGGameObject.h"
#include "Code/GameObjects/Block.h"

#include <vector>

class LivesTracker : public SGGameObject
{
public:
	LivesTracker(int id);

	int GetCurrentLives();
	int AddLife();
	int RemoveLife();
	int ResetStartingLives();

	void SetStartingLives(int startingLives);
	void SetLifeIcons(std::vector<Block*>* lifeIcons);

protected:
	int startingLives;
	int lives;
	std::vector<Block*>* lifeIcons;
};

