#pragma once

#include "Code/GameObjects/SGGameObject.h"

class LivesTracker;
class SGWindowManager;

class GameOverBar : public SGGameObject
{
public:
	GameOverBar(int id);

	void SetLivesTracker(LivesTracker* livesTracker);
	void SetWindowManager(SGWindowManager* windowManager);

private:
	virtual void OnCollision(SGGameObject& other) override;

	LivesTracker* livesTracker;
	SGWindowManager* windowManager;
};