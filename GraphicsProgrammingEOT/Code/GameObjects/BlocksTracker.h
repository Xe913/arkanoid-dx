#pragma once

#include "Code/GameObjects/SGGameObject.h"

class BlocksTracker : public SGGameObject
{
public:
	BlocksTracker(int id);

	void SetNumberOfBlocks(int blocks);
	int GetNumberOfBlocks();
	int GetCurrentNumberOfBlocks();

	void BlockDestroyed();

private:
	int blocks;
	int currentBlocks;
};

