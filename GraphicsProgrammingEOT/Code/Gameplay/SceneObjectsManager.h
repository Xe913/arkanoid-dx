#pragma once

#include "Code/Utils/SGGameObjectFactory.h"
#include "Code/GameObjects/PlayerBar.h"
#include "Code/GameObjects/Ball.h"
#include "Code/GameObjects/Block.h"
#include "Code/GameObjects/GameOverBar.h"
#include "Code/GameObjects/LivesTracker.h"
#include "Code/GameObjects/BlocksTracker.h"
#include "Code/Physics/PlayerBarCollisionDetector.h"
#include "Code/Physics/BallCollisionDetector.h"
#include "Code/GameObjects/PlayerController.h"

class SGWindowManager;
class Block;

class SceneObjectsManager
{
public:
	SceneObjectsManager(ID3D11Device& device, DirectX::SpriteBatch& spriteBatch, SGWindowManager* windowManager);
	~SceneObjectsManager();

	void InitFirstScene();

	void InitPermanentObjects();

	void InitLevel();
	void DestroyLevel();

	void SetControlSystem(DirectX::Mouse& mouse);
	void SetControlSystem(DirectX::Keyboard& keyboard);

	void PerformUpdateCycle(float deltaTime);
	void PreUpdateObjects(float deltaTime);
	void MoveObjects(float deltaTime);
	void UpdateObjects(float deltaTime);
	void RenderSceneObjects(DirectX::SimpleMath::Vector2 screenPosition);
	void ResetSceneObjectsSRV();

private:
	ID3D11Device& device;
	DirectX::SpriteBatch& spriteBatch;

	PlayerBar* playerBar;
	Ball* ball;
	std::vector<Block*> blocks;
	std::vector<SGGameObject*> borders;
	GameOverBar* gameOverBar;

	PlayerController* playerController;

	PlayerBarCollisionDetector* playerBarCollisionDetector;
	BallCollisionDetector* ballCollisionDetector;

	std::vector<Block*> lifeIcons;
	LivesTracker* livesTracker;
	BlocksTracker* blocksTracker;

	std::vector<SGGameObject*> sceneObjects;

	SGWindowManager* windowManager;

	int level;
	std::vector<std::vector<int>> blockPatterns;
	std::vector<std::vector<DirectX::XMVECTORF32>> colorSets;

	bool sceneActive;
};

