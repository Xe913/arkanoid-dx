#pragma once

#include "Code/Utils/ArkanoidConstants.h"
#include "Code/Utils/SGGameObjectFactory.h"
#include "Code/GameObjects/PlayerBar.h"

class PlayerBarFactory
{
public:
	static PlayerBar* CreatePlayer(ID3D11Device& device, wchar_t* graphicDataPath, float xPosition, float yPosition, float maxSpeed);
	static PlayerBar* CreatePlayer(ID3D11Device& device, wchar_t* graphicDataPath, DirectX::SimpleMath::Vector2 position, float maxSpeed);
};

