#pragma once

#include "Code/Utils/ArkanoidConstants.h"
#include "Code/GameObjects/Block.h"

#include <vector>

class SGGameObjectFactory;
class SGWindowManager;

class UIGenerator
{
public:
	static std::vector<Block*> CreateLivesUI(
		ID3D11Device& device, wchar_t* graphicDataPath,
		const SGWindowManager* windowData,
		int startingLives = STARTING_LIVES,
		int maxLives = MAX_DISPLAYABLE_LIVES);
};

