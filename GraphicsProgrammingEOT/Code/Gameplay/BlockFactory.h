#pragma once

#include "Code/Utils/ArkanoidConstants.h"
#include "Code/GameObjects/Block.h"
#include "Code/Utils/SGGameObjectFactory.h"

class BlockFactory
{
public:
	static Block* CreateBlock(ID3D11Device& device, wchar_t* graphicDataPath,
		float xPosition, float yPosition, int score = 1, int hits = 1,
		std::vector<DirectX::XMVECTORF32> colorSet = { 1, DirectX::Colors::White },
		int wUnits = BLOCK_NUM_TILES_W, int hUnits = BLOCK_NUM_TILES_H);

	static Block* CreateBlock(ID3D11Device& device, wchar_t* graphicDataPath,
		DirectX::SimpleMath::Vector2 position, int score = 1, int hits = 1,
		std::vector<DirectX::XMVECTORF32> colorSet = {1, DirectX::Colors::White },
		int wUnits = BLOCK_NUM_TILES_W, int hUnits = BLOCK_NUM_TILES_H);
};

