#include "pch.h"
#include "BlockFactory.h"

using namespace std;
using namespace DirectX;
using namespace DirectX::SimpleMath;

Block* BlockFactory::CreateBlock(ID3D11Device& device, wchar_t* graphicDataPath, float xPosition, float yPosition, int score, int hits, vector<XMVECTORF32> colorSet, int wUnits, int hUnits)
{
	return CreateBlock(device, graphicDataPath, Vector2(xPosition, yPosition), score, hits, colorSet, wUnits, hUnits);
}

Block* BlockFactory::CreateBlock(ID3D11Device& device, wchar_t* graphicDataPath, Vector2 position, int score, int hits, vector<XMVECTORF32> colorSet, int wUnits, int hUnits)
{
	Block* block = SGGameObjectFactory::CreatePhysicalStaticNonUpdatingGameObject<Block>(CollisionLayer::Blocks, BLOCK_TILE_W * wUnits, BLOCK_TILE_H * hUnits, position);
	block->SetupGraphics(device, graphicDataPath);
	block->SetColorSet(colorSet);
	block->SetScore(score);
	block->SetHits(hits);
	return block;
}
