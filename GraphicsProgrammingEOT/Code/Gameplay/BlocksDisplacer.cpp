#include "pch.h"
#include "BlocksDisplacer.h"

#include "Code/GameObjects/Block.h"
#include "Code/Utils/ArkanoidConstants.h"
#include "Code/Utils/SGWindowManager.h"

#include "Code/Gameplay/BlockFactory.h"

using namespace std;
using namespace DirectX;
using namespace DirectX::SimpleMath;

vector<Block*> BlocksDisplacer::GeneratePattern(
	ID3D11Device& device, wchar_t* graphicDataPath,
	const SGWindowManager* windowData, int rows, int columns, float topOffset,
	vector<vector<XMVECTORF32>> colors, vector<int> blockPattern)
{
	if (colors.size() < 1)
	{
		colors = { { Colors::White }, { Colors::LightGray } };
	}

	float screenTopOffset = windowData->GetPositionOnScreen(Vector2(0, topOffset)).y;
	Vector2 topLeftPosition = Vector2(BLOCK_W * (columns / 2), screenTopOffset);

	vector<Block*> blockWall;

	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < columns; j++)
		{
			Vector2 position = topLeftPosition + Vector2(-BLOCK_W * j, -BLOCK_H * i);
			int blockPatternIndex = (i * columns + j) % blockPattern.size();
			int blockHits = blockPattern[blockPatternIndex];

			if (blockHits != 0)
			{
				Block* block = BlockFactory::CreateBlock(device, graphicDataPath, position, blockHits * 3, blockHits, colors[(i + j) % 2]);
				blockWall.push_back(block);
			}
		}
	}

	return blockWall;
}
