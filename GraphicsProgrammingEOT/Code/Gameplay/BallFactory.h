#pragma once

#include "Code/Utils/ArkanoidConstants.h"
#include "Code/Utils/SGGameObjectFactory.h"
#include "Code/GameObjects/Ball.h"

class BallFactory
{
public:
	static Ball* CreateBall(
		ID3D11Device& device, wchar_t* graphicDataPath,
		float xPosition, float yPosition, float speed,
		DirectX::SimpleMath::Vector2 startingDirection = DirectX::SimpleMath::Vector2::Zero);

	static Ball* CreateBall(
		ID3D11Device& device, wchar_t* graphicDataPath,
		DirectX::SimpleMath::Vector2 position, float speed,
		DirectX::SimpleMath::Vector2 startingDirection = DirectX::SimpleMath::Vector2::Zero);
};

