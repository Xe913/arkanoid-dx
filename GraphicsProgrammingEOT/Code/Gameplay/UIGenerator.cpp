#include "pch.h"
#include "UIGenerator.h"

#include "Code/Utils/SGWindowManager.h"
#include "Code/Gameplay/BlockFactory.h"

using namespace std;
using namespace DirectX;
using namespace DirectX::SimpleMath;

std::vector<Block*> UIGenerator::CreateLivesUI(
	ID3D11Device& device, wchar_t* graphicDataPath,
	const SGWindowManager* windowData,
	int startingLives, int maxLives)
{
	if (maxLives > MAX_DISPLAYABLE_LIVES) maxLives = MAX_DISPLAYABLE_LIVES;
	if (startingLives > maxLives) startingLives = maxLives;
	
	vector<Block*> lifeIcons;

	for (int i = 0; i < maxLives; i++)
	{
		Vector2 position = windowData->GetPositionOnScreen(Vector2((BLOCK_TILE_W + UI_ICON_SEPARATION) * i, 0) + UI_TOP_LEFT_OFFSET);
		//placeholder. In a complete game life UI won't be composed of not-interactable blocks
		Block* lifeIcon = BlockFactory::CreateBlock(
			device, graphicDataPath, position,
			0, 999, {1, Colors::DarkRed }, 1, 1);
		lifeIcon->SetCanCollide(false);
		if (i >= startingLives) lifeIcon->SetHasGraphics(false);
		lifeIcons.push_back(lifeIcon);
	}

	return lifeIcons;
}
