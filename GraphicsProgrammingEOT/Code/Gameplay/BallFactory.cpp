#include "pch.h"
#include "BallFactory.h"

using namespace DirectX;
using namespace DirectX::SimpleMath;

Ball* BallFactory::CreateBall(ID3D11Device& device, wchar_t* graphicDataPath, float xPosition, float yPosition, float speed, Vector2 startingDirection)
{
    return CreateBall(device, graphicDataPath, Vector2(xPosition, yPosition), speed, startingDirection);
}

Ball* BallFactory::CreateBall(ID3D11Device& device, wchar_t* graphicDataPath, Vector2 position, float speed, Vector2 startingDirection)
{
	Ball* ball = SGGameObjectFactory::CreatePhysicalDynamicNonUpdatingGameObject<Ball>(CollisionLayer::Ball, BALL_RADIUS, BALL_RADIUS, position);
	ball->SetMaxSpeed(speed);
	ball->SetDimensions(ball->GetDimensions());
	ball->SetVelocity(startingDirection * speed);
	ball->SetupGraphics(device, graphicDataPath);
    return ball;
}
