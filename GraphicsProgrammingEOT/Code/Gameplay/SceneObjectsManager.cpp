#include "pch.h"
#include "SceneObjectsManager.h"

#include "Code/Utils/ArkanoidConstants.h"
#include "Code/Utils/SGWindowManager.h"

#include "Code/Gameplay/PlayerBarFactory.h"
#include "Code/Gameplay/BallFactory.h"
#include "Code/Gameplay/BlockFactory.h"
#include "Code/Gameplay/BlocksDisplacer.h"
#include "Code/Gameplay/UIGenerator.h"

#include "Code/Physics/PlayerBarCollisionDetector.h"
#include "Code/Physics/BallCollisionDetector.h"

#include "Code/GameObjects/PlayerController.h"

#include "Code/Physics/SGCollisionFramework.h"

#include <random>

using namespace std;
using namespace DirectX;
using namespace DirectX::SimpleMath;

SceneObjectsManager::SceneObjectsManager(ID3D11Device& device, SpriteBatch& spriteBatch, SGWindowManager* windowManager)
	: device(device), spriteBatch(spriteBatch), windowManager(windowManager), level(0), sceneActive(false),
	playerController(nullptr), playerBarCollisionDetector(nullptr), ballCollisionDetector(nullptr),
	playerBar(nullptr), ball(nullptr), gameOverBar(nullptr), livesTracker(nullptr), blocksTracker(nullptr)
{
	CollisionMatrix::InitializeMatrix();
}

SceneObjectsManager::~SceneObjectsManager()
{
	delete ballCollisionDetector;
	delete playerBarCollisionDetector;
	delete playerController;

	delete blocksTracker;
	delete livesTracker;

	for (int i = 0; i < lifeIcons.size(); i++)
		delete lifeIcons[i];
	lifeIcons.clear();

	delete gameOverBar;

	for (int i = 0; i < borders.size(); i++)
		delete borders[i];
	borders.clear();

	for (int i = 0; i < blocks.size(); i++)
		delete blocks[i];
	blocks.clear();

	delete ball;
	delete playerBar;

	sceneObjects.clear();
}

void SceneObjectsManager::InitFirstScene()
{
	blockPatterns = { LV1_BLOCK_PATTERN, LV2_BLOCK_PATTERN, LV3_BLOCK_PATTERN };

	InitPermanentObjects();
	InitLevel();
	sceneActive = true;
}

void SceneObjectsManager::InitPermanentObjects()
{
	/*
	these objects work the same way in every level. In this version the player bar, the ball and the level borders do too
	but they could theoretically be moved or replaced by similar objects with different logic in more advanced levels.
	For this reason, they're initialized alongside the other level-bound objects and not here.
	*/
	playerController = SGGameObjectFactory::CreateLogicNonUpdatingGameObject<PlayerController>();
	playerController->SetCanPreUpdate(true);
	playerController->SetCanUpdate(false);
	playerController->SetWindowManager(windowManager);

	playerBarCollisionDetector = SGGameObjectFactory::CreateLogicGameObject<PlayerBarCollisionDetector>();
	playerBarCollisionDetector->SetWindowManager(windowManager);

	ballCollisionDetector = SGGameObjectFactory::CreateLogicGameObject<BallCollisionDetector>();
}

void SceneObjectsManager::InitLevel()
{
	sceneObjects.push_back(playerController);
	sceneObjects.push_back(playerBarCollisionDetector);
	sceneObjects.push_back(ballCollisionDetector);

	Vector2 playerStartingPosition = windowManager->GetPositionOnScreen(PLAYER_RELATIVE_STARTING_POS * windowManager->GetDimensions());

	playerBar = PlayerBarFactory::CreatePlayer(
		device, PLAYER_BAR_RESOURCE,
		playerStartingPosition,
		PLAYER_MAX_SPEED);
	sceneObjects.push_back(playerBar);

	Vector2 ballPos = Ball::GetRandomStartingPosition(windowManager->GetDimensions());
	Vector2 ballStartingPosition = windowManager->GetPositionOnScreen(ballPos);
	Vector2 ballStartingDirection = Ball::GetRandomStartingDirection();

	ball = BallFactory::CreateBall(
		device, BALL_RESOURCE,
		ballStartingPosition,
		BALL_SPEED,
		ballStartingDirection);
	sceneObjects.push_back(ball);

	blocksTracker = SGGameObjectFactory::CreateLogicNonUpdatingGameObject<BlocksTracker>();
	sceneObjects.push_back(blocksTracker);

	blocks = BlocksDisplacer::GeneratePattern(
		device, BLOCK_RESOURCE, windowManager,
		WALL_ROWS, WALL_COLUMNS,
		WALL_TOP_OFFSET,
		COLOR_PALETTES,
		blockPatterns[level]);
	for (int i = 0; i < blocks.size(); i++)
	{
		blocks[i]->AddTracker(blocksTracker);
		sceneObjects.push_back(blocks[i]);
	}

	borders.push_back(
		SGGameObjectFactory::CreatePhysicalStaticNonUpdatingGameObject<SGGameObject>(
			CollisionLayer::Blocks, Vector2(windowManager->GetWidth(), BORDER_WIDTH),
			Vector2(0, (windowManager->GetHeight() + BORDER_WIDTH) / 2)));
	borders.push_back(
		SGGameObjectFactory::CreatePhysicalStaticNonUpdatingGameObject<SGGameObject>(
			CollisionLayer::Blocks, Vector2(BORDER_WIDTH, windowManager->GetHeight()),
			Vector2((windowManager->GetWidth() + BORDER_WIDTH) / 2, 0)));
	borders.push_back(
		SGGameObjectFactory::CreatePhysicalStaticNonUpdatingGameObject<SGGameObject>(
			CollisionLayer::Blocks, Vector2(BORDER_WIDTH, windowManager->GetHeight()),
			Vector2(-(windowManager->GetWidth() + BORDER_WIDTH) / 2, 0)));

	borders[0]->SetHasGraphics(false);
	borders[1]->SetHasGraphics(false);
	borders[2]->SetHasGraphics(false);
	sceneObjects.push_back(borders[0]);
	sceneObjects.push_back(borders[1]);
	sceneObjects.push_back(borders[2]);

	gameOverBar = SGGameObjectFactory::CreatePhysicalStaticGameObject<GameOverBar>(
		CollisionLayer::Blocks, Vector2(windowManager->GetWidth(), BORDER_WIDTH),
		Vector2(0, -(windowManager->GetHeight() + BORDER_WIDTH) / 2));
	gameOverBar->SetWindowManager(windowManager);
	gameOverBar->SetHasGraphics(false);
	sceneObjects.push_back(gameOverBar);

	livesTracker = SGGameObjectFactory::CreateLogicNonUpdatingGameObject<LivesTracker>();
	//redundant. Lives tracker build itself with STARTING_LIVES lives
	//but I don't know, if it's going to change in the future
	livesTracker->SetStartingLives(STARTING_LIVES);
	livesTracker->ResetStartingLives();
	sceneObjects.push_back(livesTracker);

	lifeIcons = UIGenerator::CreateLivesUI(device, LIFE_ICON_RESOURCE, windowManager);
	for (int i = 0; i < lifeIcons.size(); i++)
		sceneObjects.push_back(lifeIcons[i]);

	playerController->AddControlledObject(playerBar);
	playerBarCollisionDetector->SetPlayerBar(playerBar);
	ballCollisionDetector->SetBall(ball);
	gameOverBar->SetLivesTracker(livesTracker);
	livesTracker->SetLifeIcons(&lifeIcons);
	blocksTracker->SetNumberOfBlocks(blocks.size());
	ballCollisionDetector->SetSceneObjects(&sceneObjects);
}

void SceneObjectsManager::DestroyLevel()
{
	playerController->ResetControlledObjects();

	for (int i = 0; i < lifeIcons.size(); i++)
		delete lifeIcons[i];
	lifeIcons.clear();

	delete livesTracker;
	delete gameOverBar;

	for (int i = 0; i < borders.size(); i++)
		delete borders[i];
	borders.clear();

	for (int i = 0; i < blocks.size(); i++)
		delete blocks[i];
	blocks.clear();

	delete ball;
	delete playerBar;

	sceneObjects.clear();
}

void SceneObjectsManager::SetControlSystem(DirectX::Mouse& mouse)
{
	playerController->SetControlSystem(mouse);
}

void SceneObjectsManager::SetControlSystem(DirectX::Keyboard& keyboard)
{
	playerController->SetControlSystem(keyboard);
}

void SceneObjectsManager::PerformUpdateCycle(float deltaTime)
{
	if (sceneActive)
	{
		PreUpdateObjects(deltaTime);
		UpdateObjects(deltaTime);
		MoveObjects(deltaTime);

		//of course, in a complete game levels won't cycle back to zero
		if (livesTracker->GetCurrentLives() < 0 || level >= blockPatterns.size() - 1)
		{
			DestroyLevel();
			level = 0;
			InitLevel();
		}
		else if (blocksTracker->GetCurrentNumberOfBlocks() <= 0)
		{
			DestroyLevel();
			level++;
			InitLevel();
		}
	}
}

void SceneObjectsManager::PreUpdateObjects(float deltaTime)
{
	for (int i = 0; i < sceneObjects.size(); i++)
		sceneObjects[i]->TryPreUpdate(deltaTime);
}

void SceneObjectsManager::UpdateObjects(float deltaTime)
{
	for (int i = 0; i < sceneObjects.size(); i++)
		sceneObjects[i]->TryUpdate(deltaTime);
}

void SceneObjectsManager::MoveObjects(float deltaTime)
{
	for (int i = 0; i < sceneObjects.size(); i++)
		sceneObjects[i]->TryMove(deltaTime);
}

void SceneObjectsManager::RenderSceneObjects(Vector2 screenPosition)
{
	for (int i = 0; i < sceneObjects.size(); i++)
		sceneObjects[i]->TryRender(spriteBatch, screenPosition);
}

void SceneObjectsManager::ResetSceneObjectsSRV()
{
	for (int i = 0; i < sceneObjects.size(); i++)
		sceneObjects[i]->ResetSRV();
}
