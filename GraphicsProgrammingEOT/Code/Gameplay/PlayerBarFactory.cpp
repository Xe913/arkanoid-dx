#include "pch.h"
#include "PlayerBarFactory.h"

using namespace DirectX;
using namespace DirectX::SimpleMath;

PlayerBar* PlayerBarFactory::CreatePlayer(ID3D11Device& device, wchar_t* graphicDataPath, float xPosition, float yPosition, float maxSpeed)
{
    return CreatePlayer(device, graphicDataPath, Vector2(xPosition, yPosition), maxSpeed);
}

PlayerBar* PlayerBarFactory::CreatePlayer(ID3D11Device& device, wchar_t* graphicDataPath, Vector2 position, float maxSpeed)
{
	PlayerBar* playerBar = SGGameObjectFactory::CreatePhysicalDynamicNonUpdatingGameObject<PlayerBar>(CollisionLayer::Player, PLAYER_DIMENSIONS, position);
	playerBar->LockVerticalMovement();
	playerBar->SetMaxSpeed(maxSpeed);
	playerBar->SetupGraphics(device, graphicDataPath);
	playerBar->SetColor(PLAYER_BAR_COLOR);
	return playerBar;
}