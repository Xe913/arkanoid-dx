#pragma once

#include "Code/Utils/SGGameObjectFactory.h"
#include "Code/GameObjects/Block.h"

#include <vector>

class SGGameObject;
class SGWindowManager;

class BlocksDisplacer
{
public:
	static std::vector<Block*> GeneratePattern(
		ID3D11Device& device, wchar_t* graphicDataPath,
		const SGWindowManager* windowData, int rows, int columns, float topOffset,
		std::vector<std::vector<DirectX::XMVECTORF32>> colors = { { DirectX::Colors::White }, { DirectX::Colors::LightGray } },
		std::vector<int> blockPattern = std::vector<int>(1, 1));
};

