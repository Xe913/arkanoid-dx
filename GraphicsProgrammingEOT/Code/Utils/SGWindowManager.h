#pragma once

class SGWindowManager
{
public:
	SGWindowManager();
	SGWindowManager(DirectX::SimpleMath::Vector2 dimensions);
	SGWindowManager(int width, int height);
	DirectX::SimpleMath::Vector2 GetDimensions() const;
	DirectX::SimpleMath::Vector2 GetScreenCenter() const;
	DirectX::SimpleMath::Vector2 GetPositionOnScreen(DirectX::SimpleMath::Vector2 position) const;
	int GetWidth() const;
	int GetHeight() const;
	void SetDimensions(DirectX::SimpleMath::Vector2 dimensions);
	void SetWidth(int width);
	void SetHeight(int height);

private:
	DirectX::SimpleMath::Vector2 dimensions;
};

