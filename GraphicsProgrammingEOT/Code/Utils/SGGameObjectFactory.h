#pragma once

#include "Code/Physics/SGCollisionFramework.h"
#include "Code/GameObjects/SGGameObject.h"

namespace
{
	int nextID;
}

class SGGameObjectFactory
{
public:
	SGGameObjectFactory() : SGGameObjectFactory(0) { }

	SGGameObjectFactory(int firstObjectID)
	{
		nextID = firstObjectID;
	}

#pragma region LOGIC_OBJECTS

	template<class SGGameObjectClass>
	static SGGameObjectClass* CreateLogicGameObject()
	{
		SGGameObjectClass* gameObject = CreateLogicNonUpdatingGameObject<SGGameObjectClass>();
		gameObject->SetCanUpdate(true);
		return gameObject;
	}

	template<class SGGameObjectClass>
	static SGGameObjectClass* CreateLogicNonUpdatingGameObject()
	{
		return CreateDefaultGameObject<SGGameObjectClass>();
	}

#pragma endregion LOGIC_OBJECTS

#pragma region GRAPHIC_OBJECTS
	template<class SGGameObjectClass>
	static SGGameObjectClass* CreateStaticGameObject(DirectX::SimpleMath::Vector2 dimensions, DirectX::SimpleMath::Vector2 position)
	{
		return CreateStaticGameObject<SGGameObjectClass>(dimensions.x, dimensions.y, position);
	}

	template<class SGGameObjectClass>
	static SGGameObjectClass* CreateStaticGameObject(float width, float height, DirectX::SimpleMath::Vector2 position)
	{
		SGGameObjectClass* gameObject = CreateStaticNonUpdatingGameObject<SGGameObjectClass>(width, height, position);
		gameObject->SetCanUpdate(true);
		return gameObject;
	}

	template<class SGGameObjectClass>
	static SGGameObjectClass* CreateDynamicGameObject(DirectX::SimpleMath::Vector2 dimensions, DirectX::SimpleMath::Vector2 position)
	{
		return CreateDynamicGameObject<SGGameObjectClass>(dimensions.x, dimensions.y, position);
	}

	template<class SGGameObjectClass>
	static SGGameObjectClass* CreateDynamicGameObject(float width, float height, DirectX::SimpleMath::Vector2 position)
	{
		SGGameObjectClass* gameObject = CreateDynamicNonUpdatingGameObject<SGGameObjectClass>(width, height, position);
		gameObject->SetCanUpdate(true);
		return gameObject;
	}

	template<class SGGameObjectClass>
	static SGGameObjectClass* CreateStaticNonUpdatingGameObject(DirectX::SimpleMath::Vector2 dimensions, DirectX::SimpleMath::Vector2 position)
	{
		return CreateStaticNonUpdatingGameObject<SGGameObjectClass>(dimensions.x, dimensions.y, position);
	}

	template<class SGGameObjectClass>
	static SGGameObjectClass* CreateStaticNonUpdatingGameObject(float width, float height, DirectX::SimpleMath::Vector2 position)
	{
		SGGameObjectClass* gameObject = CreateDefaultGameObject<SGGameObjectClass>();
		gameObject->SetPosition(position);
		gameObject->SetWidth(width);
		gameObject->SetHeight(height);
		gameObject->SetHasGraphics(true);
		return gameObject;
	}

	template<class SGGameObjectClass>
	static SGGameObjectClass* CreateDynamicNonUpdatingGameObject(DirectX::SimpleMath::Vector2 dimensions, DirectX::SimpleMath::Vector2 position)
	{
		return CreateDynamicNonUpdatingGameObject<SGGameObjectClass>(dimensions.x, dimensions.y, position);
	}

	template<class SGGameObjectClass>
	static SGGameObjectClass* CreateDynamicNonUpdatingGameObject(float width, float height, DirectX::SimpleMath::Vector2 position)
	{
		SGGameObjectClass* gameObject = CreateStaticNonUpdatingGameObject<SGGameObjectClass>(width, height, position);
		gameObject->SetCanMove(true);
		return gameObject;
	}

#pragma endregion GRAPHIC_OBJECTS

#pragma region PHYSICAL_OBJECTS

	template<class SGGameObjectClass>
	static SGGameObjectClass* CreatePhysicalStaticGameObject(CollisionLayer layer, DirectX::SimpleMath::Vector2 dimensions, DirectX::SimpleMath::Vector2 position)
	{
		return CreatePhysicalStaticGameObject<SGGameObjectClass>(layer, dimensions.x, dimensions.y, position);
	}

	template<class SGGameObjectClass>
	static SGGameObjectClass* CreatePhysicalStaticGameObject(CollisionLayer layer, float width, float height, DirectX::SimpleMath::Vector2 position)
	{
		SGGameObjectClass* gameObject = CreatePhysicalStaticNonUpdatingGameObject<SGGameObjectClass>(layer, width, height, position);
		gameObject->SetCanUpdate(true);
		return gameObject;
	}

	template<class SGGameObjectClass>
	static SGGameObjectClass* CreatePhysicalDynamicGameObject(CollisionLayer layer, DirectX::SimpleMath::Vector2 dimensions, DirectX::SimpleMath::Vector2 position)
	{
		return CreatePhysicalDynamicGameObject<SGGameObjectClass>(layer, dimensions.x, dimensions.y, position);
	}

	template<class SGGameObjectClass>
	static SGGameObjectClass* CreatePhysicalDynamicGameObject(CollisionLayer layer, float width, float height, DirectX::SimpleMath::Vector2 position)
	{
		SGGameObjectClass* gameObject = CreatePhysicalDynamicNonUpdatingGameObject<SGGameObjectClass>(layer, width, height, position);
		gameObject->SetCanUpdate(true);
		return gameObject;
	}

	template<class SGGameObjectClass>
	static SGGameObjectClass* CreatePhysicalStaticNonUpdatingGameObject(CollisionLayer layer, DirectX::SimpleMath::Vector2 dimensions, DirectX::SimpleMath::Vector2 position)
	{
		return CreatePhysicalStaticNonUpdatingGameObject<SGGameObjectClass>(layer, dimensions.x, dimensions.y, position);
	}

	template<class SGGameObjectClass>
	static SGGameObjectClass* CreatePhysicalStaticNonUpdatingGameObject(CollisionLayer layer, float width, float height, DirectX::SimpleMath::Vector2 position)
	{
		SGGameObjectClass* gameObject = CreateStaticNonUpdatingGameObject<SGGameObjectClass>(width, height, position);
		gameObject->SetCanCollide(true);
		gameObject->SetLayer(layer);
		return gameObject;
	}

	template<class SGGameObjectClass>
	static SGGameObjectClass* CreatePhysicalDynamicNonUpdatingGameObject(CollisionLayer layer, DirectX::SimpleMath::Vector2 dimensions, DirectX::SimpleMath::Vector2 position)
	{
		return CreatePhysicalDynamicNonUpdatingGameObject<SGGameObjectClass>(layer, dimensions.x, dimensions.y, position);
	}

	template<class SGGameObjectClass>
	static SGGameObjectClass* CreatePhysicalDynamicNonUpdatingGameObject(CollisionLayer layer, float width, float height, DirectX::SimpleMath::Vector2 position)
	{
		SGGameObjectClass* gameObject = CreateDynamicNonUpdatingGameObject<SGGameObjectClass>(width, height, position);
		gameObject->SetCanCollide(true);
		gameObject->SetLayer(layer);
		return gameObject;
	}
#pragma endregion PHYSICAL_OBJECTS

private:
	template<class SGGameObjectClass>
	static SGGameObjectClass* CreateDefaultGameObject()
	{
		static_assert(std::is_base_of<SGGameObject, SGGameObjectClass>::value, "The object you're trying to instantiate is not a SGGameObject.");
		return new SGGameObjectClass(nextID++);
	}
};

