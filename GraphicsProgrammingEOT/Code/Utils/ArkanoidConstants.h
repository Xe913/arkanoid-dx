#pragma once

//RESOURCES-------------------------------------

#define PLAYER_BAR_RESOURCE L"GraphicResources/player_bar.png"
#define BALL_RESOURCE L"GraphicResources/ball.png"
#define BLOCK_RESOURCE L"GraphicResources/block.png"
#define LIFE_ICON_RESOURCE L"GraphicResources/life_icon.png"

//GAMEPLAY--------------------------------------

#define WALL_ROWS 5
#define WALL_COLUMNS 7

#define STARTING_LIVES 3

#define BALL_SPEED 300
#define BALL_RELATIVE_STARTING_POS_X 0.65
#define BALL_LAUNCH_VARIATION 0.75
#define BALL_BOUNCE_VARIATION 0.1

#define PLAYER_MAX_SPEED -1
#define PLAYER_RELATIVE_STARTING_POS_X 0
#define PLAYER_RELATIVE_STARTING_POS_Y 0.9
#define PLAYER_RELATIVE_STARTING_POS DirectX::SimpleMath::Vector2(PLAYER_RELATIVE_STARTING_POS_X, PLAYER_RELATIVE_STARTING_POS_Y)

#define WALL_TOP_OFFSET 40

#define PLAYER_BAR_COLOR Colors::Aqua

//DIMENSIONS------------------------------------

#define BALL_RADIUS 16

#define PLAYER_W 128
#define PLAYER_H 24
#define PLAYER_DIMENSIONS DirectX::SimpleMath::Vector2(PLAYER_W, PLAYER_H)

#define BLOCK_TILE_W 16
#define BLOCK_TILE_H 16
#define BLOCK_NUM_TILES_W 6
#define BLOCK_NUM_TILES_H 2

#define BLOCK_W BLOCK_TILE_W * BLOCK_NUM_TILES_W
#define BLOCK_H BLOCK_TILE_H * BLOCK_NUM_TILES_H

#define BORDER_WIDTH 100

#define WINDOW_DEFAULT_W 800
#define WINDOW_DEFAULT_H 600

//UI--------------------------------------------

#define MAX_DISPLAYABLE_LIVES 9
#define UI_TOP_OFFSET 20
#define UI_LEFT_OFFSET 20
#define UI_TOP_LEFT_OFFSET DirectX::SimpleMath::Vector2(UI_LEFT_OFFSET, UI_TOP_OFFSET)
#define UI_ICON_SEPARATION 8


//LEVEL-PATTERS---------------------------------

#define LV1_BLOCK_PATTERN { 0, 1, 1, 1, 1, 1, 0,   0, 1, 1, 1, 1, 1, 0 }

#define LV2_BLOCK_PATTERN { 2, 0, 1, 1, 1, 0, 2,   0, 1, 1, 2, 1, 1, 0,   1, 1, 2, 2, 2, 1, 1,   0, 1, 1, 2, 1, 1, 0,   2, 0, 1, 1, 1, 0, 2 }

#define LV3_BLOCK_PATTERN { 1, 1, 1, 1, 1, 1, 1,   1, 1, 1, 1, 1, 1, 1,   1, 2, 2, 2, 2, 2, 1 }

#define COLOR_PALETTE1 { Colors::CadetBlue, Colors::Blue }
#define COLOR_PALETTE2 { Colors::DarkSalmon, Colors::DarkRed }
#define COLOR_PALETTES { COLOR_PALETTE1, COLOR_PALETTE2 }