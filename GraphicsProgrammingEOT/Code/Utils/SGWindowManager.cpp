#include "pch.h"
#include "SGWindowManager.h"

using namespace DirectX;
using namespace DirectX::SimpleMath;

SGWindowManager::SGWindowManager()
    : dimensions(Vector2::Zero)
{ }

SGWindowManager::SGWindowManager(Vector2 dimensions)
    : dimensions(dimensions)
{ }

SGWindowManager::SGWindowManager(int width, int height)
    : SGWindowManager(Vector2(width, height))
{ }

DirectX::SimpleMath::Vector2 SGWindowManager::GetDimensions() const
{
    return dimensions;
}

DirectX::SimpleMath::Vector2 SGWindowManager::GetScreenCenter() const
{
    return dimensions / 2;
}

DirectX::SimpleMath::Vector2 SGWindowManager::GetPositionOnScreen(DirectX::SimpleMath::Vector2 position) const
{
    return - position + GetScreenCenter();
}

int SGWindowManager::GetWidth() const
{
    return dimensions.x;
}

int SGWindowManager::GetHeight() const
{
    return dimensions.y;
}

void SGWindowManager::SetDimensions(DirectX::SimpleMath::Vector2 dimensions)
{
    this->dimensions = dimensions;
}

void SGWindowManager::SetWidth(int width)
{
    dimensions.x = width;
}

void SGWindowManager::SetHeight(int height)
{
    dimensions.y = height;
}
