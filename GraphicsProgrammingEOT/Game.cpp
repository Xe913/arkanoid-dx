#include "pch.h"
#include "Game.h"

#include "Code/Utils/ArkanoidConstants.h"

extern void ExitGame() noexcept;

using namespace std;
using namespace DirectX;
using namespace DirectX::SimpleMath;

using Microsoft::WRL::ComPtr;

Game::Game() noexcept(false)
{
    deviceResources = std::make_unique<DX::DeviceResources>();
    deviceResources->RegisterDeviceNotify(this);
}

void Game::Initialize(HWND window, int width, int height)
{
    deviceResources->SetWindow(window, width, height);
    windowManager = new SGWindowManager(width, height);

    deviceResources->CreateDeviceResources();
    CreateDeviceDependentResources();

    deviceResources->CreateWindowSizeDependentResources();
    CreateWindowSizeDependentResources();

    mouse = make_unique<Mouse>();
	mouse->SetWindow(window);
    sceneObjectsManager->SetControlSystem(*mouse);

    /*
    m_timer.SetFixedTimeStep(true);
    m_timer.SetTargetElapsedSeconds(1.0 / 60);
    */
}

#pragma region Frame Update
void Game::Tick()
{
    timer.Tick([&]()
    {
        Update(timer);
    });

    Render();
}

// Updates the world.
void Game::Update(DX::StepTimer const& timer)
{
    float elapsedTime = float(timer.GetElapsedSeconds());
    sceneObjectsManager->PerformUpdateCycle(elapsedTime);
}
#pragma endregion

#pragma region Frame Render
void Game::Render()
{
    if (timer.GetFrameCount() == 0)
    {
        return;
    }

    Clear();

    deviceResources->PIXBeginEvent(L"Render");
    auto context = deviceResources->GetD3DDeviceContext();

    context;

    spriteBatch->Begin(SpriteSortMode_Deferred, nullptr, states->LinearWrap());

    sceneObjectsManager->RenderSceneObjects(screenPosition);

	spriteBatch->End();

    deviceResources->PIXEndEvent();

    deviceResources->Present();
}

// Helper method to clear the back buffers.
void Game::Clear()
{
    deviceResources->PIXBeginEvent(L"Clear");

    auto context = deviceResources->GetD3DDeviceContext();
    auto renderTarget = deviceResources->GetRenderTargetView();
    auto depthStencil = deviceResources->GetDepthStencilView();

    context->ClearRenderTargetView(renderTarget, Colors::Black);
    context->ClearDepthStencilView(depthStencil, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);
    context->OMSetRenderTargets(1, &renderTarget, depthStencil);

    auto viewport = deviceResources->GetScreenViewport();
    context->RSSetViewports(1, &viewport);

    deviceResources->PIXEndEvent();
}
#pragma endregion

#pragma region Message Handlers

void Game::OnActivated()
{

}

void Game::OnDeactivated()
{

}

void Game::OnSuspending()
{

}

void Game::OnResuming()
{
    timer.ResetElapsedTime();
}

void Game::OnWindowMoved()
{
    auto r = deviceResources->GetOutputSize();
    deviceResources->WindowSizeChanged(r.right, r.bottom);
}

void Game::OnWindowSizeChanged(int width, int height)
{
    if (!deviceResources->WindowSizeChanged(width, height))
        return;

    CreateWindowSizeDependentResources();
}

void Game::GetDefaultSize(int& width, int& height) const noexcept
{
    width = WINDOW_DEFAULT_W;
    height = WINDOW_DEFAULT_H;
}
#pragma endregion

#pragma region Direct3D Resources
void Game::CreateDeviceDependentResources()
{
    auto device = deviceResources->GetD3DDevice();

    states = std::make_unique<CommonStates>(device);

	auto context = deviceResources->GetD3DDeviceContext();
	spriteBatch = std::make_unique<SpriteBatch>(context);

    sceneObjectsManager = new SceneObjectsManager(*device, *spriteBatch, windowManager);
    sceneObjectsManager->InitFirstScene();
}

void Game::CreateWindowSizeDependentResources()
{
	auto size = deviceResources->GetOutputSize();
	screenPosition.x = float(size.right) / 2.f;
	screenPosition.y = float(size.bottom) / 2.f;
}

void Game::OnDeviceLost()
{
    sceneObjectsManager->ResetSceneObjectsSRV();
	spriteBatch.reset();
    states.reset();
}

void Game::OnDeviceRestored()
{
    CreateDeviceDependentResources();

    CreateWindowSizeDependentResources();
}
#pragma endregion
